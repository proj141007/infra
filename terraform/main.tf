terraform {
  required_providers {
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = "~> 3.0"
    }
  }
}

provider "aws" {
  region  = "eu-central-1"
}

locals {
  name        = "fcna-dev"
  region      = "eu-central-1"
  environment = "dev"
  inst_az     = "eu-central-1a"
}

#############################
# S3 bucket
#############################
resource "aws_s3_bucket" "raw_data" {
  bucket        = "${var.raw_data_bucket}"
  force_destroy = true
}

resource "aws_s3_bucket" "processed_data" {
  bucket        = "${var.static_web_bucket}"
  force_destroy = true 
}

resource "aws_s3_bucket_website_configuration" "processed_data_static_web" {
  bucket = aws_s3_bucket.processed_data.bucket

  index_document {
    suffix = "index.html"
  }
}

resource "aws_s3_bucket_policy" "processed_data_bucket_policy" {
  bucket = aws_s3_bucket.processed_data.bucket

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Sid       = "PublicReadGetObject"
        Effect    = "Allow"
        Principal = "*"
        Action    = ["s3:GetObject"]
        Resource  = ["arn:aws:s3:::${aws_s3_bucket.processed_data.bucket}/*"]
      },
    ]
  })
}

resource "aws_s3_bucket_public_access_block" "processed_data_public_access" {
  bucket = aws_s3_bucket.processed_data.id

  block_public_acls       = false
  block_public_policy     = false
  ignore_public_acls      = false
  restrict_public_buckets = false
}

#############################
# IAM Roles
#############################
resource "aws_iam_role" "prog_s3_access" {
  name               = "ProgS3Access"

  assume_role_policy = jsonencode({
    Version          = "2012-10-17"
    Statement        = [
      {
        Action       = "sts:AssumeRole"
        Effect       = "Allow"
        Principal    = {
          Service    = "ec2.amazonaws.com"
        }
      },
    ]
  })
}

resource "aws_iam_instance_profile" "iam_prog_profile" {
	name = "ProgS3Access"
	role = "${aws_iam_role.prog_s3_access.name}"
}

resource "aws_iam_policy" "s3_access" {
  name           = "s3_access_policy"
  policy         = jsonencode({
    Version      = "2012-10-17"
    Statement    = [
      {
        Action   = [
          "s3:GetObject",
          "s3:PutObject",
        ]
        Effect   = "Allow"
        Resource = [
          "${aws_s3_bucket.raw_data.arn}/*",
          "${aws_s3_bucket.processed_data.arn}/*",
        ]
      },
    ]
  })
}

resource "aws_iam_role_policy_attachment" "s3_access_attachment" {
  role       = aws_iam_role.prog_s3_access.name
  policy_arn = aws_iam_policy.s3_access.arn
}

#############################
# EC2 instance
#############################
resource "aws_security_group" "srv_security_group" {
  name = "srv_sg"
  description = "Allow 80 8001"

  dynamic "ingress" {
    for_each = var.srv_traffic_rules
    content {
     from_port   = ingress.value.from_port
     to_port     = ingress.value.to_port
     protocol    = ingress.value.protocol
     cidr_blocks = ingress.value.cidr_blocks
    }
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "development"
  }
}

resource "aws_instance" "cryptorates_server" {
    ami                    = var.ami_crypto 
    instance_type          = var.inst_size
    availability_zone      = local.inst_az
    iam_instance_profile   = aws_iam_role.prog_s3_access.name
    key_name               = var.key_name
    security_groups        = [aws_security_group.srv_security_group.name]
    root_block_device {
        volume_size        = "30"
    }

    tags = {
        Name = "development" 
    }

    user_data = "${file("init.sh")}"

    provisioner "local-exec" {
        command = "echo '${self.public_dns} ${self.public_ip}' >> hosts.list"
    }
    
    connection {
        type        = "ssh"
        user        = "ubuntu"
        private_key = "${file(var.private_key_path)}"
        timeout     = "4m"
        host        = self.public_ip
    }

    provisioner "remote-exec" {
        inline = [
            "until command -v gitlab-runner >/dev/null; do echo 'Waiting for GitLab Runner to be installed'; sleep 30; done",
            "sudo gitlab-runner register --non-interactive --url '${var.gitlab_url}' --registration-token '${var.runner_token}' --executor 'docker' --docker-image 'docker:dind' --tag-list 'crypto,docker,build' --docker-tlsverify=true --docker-privileged=true --docker-volumes '/certs' --docker-volumes '/cache' --description '${var.runner_name_docker}'",
            "sudo gitlab-runner register --non-interactive --url '${var.gitlab_url}' --registration-token '${var.runner_token}' --executor 'shell' --description '${var.runner_name_shell}' --tag-list 'crypto,shell,run'"
        ]
    }
}

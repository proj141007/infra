variable "srv_traffic_rules" {
  type = list(object({
    from_port = number
    to_port = number
    protocol = string
    cidr_blocks = list(string)
  }))
  default = [
    {
      from_port = 22
      to_port = 22
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    },
    {
      from_port = 8001
      to_port = 8001
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  ]
}


variable "ami_crypto" {
    default = "ami-0faab6bdbac9486fb"
    type = string
}

variable "private_key_path" {
    default = ""
    type = string
}

variable "inst_size" {
    default = ""
    type = string
}

variable "key_name" {
    default = ""
    type = string
}

variable "gitlab_url" {
  description = "The URL of the GitLab instance."
  type        = string
  default     = "https://gitlab.com/"
}

variable "runner_token" {
  description = "Registration token for the GitLab runner."
  type        = string
}

variable "runner_name_shell" {
  description = "GitLab runner name for shell runner"
  type        = string
  default     = "Cryptorates runner shell"

}

variable "runner_name_docker" {
  description = "GitLab runner name for docker runner"
  type        = string
  default     = "Cryptorates runner docker"
}

variable "raw_data_bucket" {
  description = "where data from coinbase will be placed>"
  type        = string
}

variable "static_web_bucket" {
  description = "where static web-site will be placed"
  type        = string
}